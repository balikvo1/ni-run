#include <stdio.h>

#include "arena.h"
#include "parser.h"

int main(int argc, char **argv) {
#ifdef _WIN32
	// Set standard output mode to "binary" on Windows.
	// https://nullprogram.com/blog/2021/12/30/
	int _setmode(int, int);
	_setmode(1, 0x8000);
#endif

	if (argc < 2) {
		fprintf(stderr, "Error: expected at least one argument\n");
		return 1;
	}

	Arena arena;
	arena_init(&arena);

	Ast *ast = parse_src(&arena, (Str) { .str = (u8 *) argv[1], .len = strlen(argv[1]) });

	if (ast == NULL) {
		fprintf(stderr, "Failed to parse source\n");
		arena_destroy(&arena);
		return 1;
	}

	printf("Hello world!\n");

	arena_destroy(&arena);

	return 0;
}
